<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentmasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departmentmaster', function (Blueprint $table) {
            $table->id('dptID');
            $table->string('dptName');
            $table->bigInteger('dptCompany')->unsigned();
            $table->string('dptStatus');
            $table->bigInteger('dptUpdated_by')->unsigned()->nullable();
            $table->foreign('dptUpdated_by')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('dptCompany')->references('cmpID')->on('companymaster')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departmentmaster');
    }
}
