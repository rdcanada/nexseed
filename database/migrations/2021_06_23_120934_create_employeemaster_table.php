<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeemasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeemaster', function (Blueprint $table) {
            $table->id('empID');
            $table->string('empFirstName');
            $table->string('empMiddleName')->nullable();
            $table->string('empLastName');
            $table->bigInteger('empDepartment');
            $table->string('empStatus');
            $table->string('empUpdated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeemaster');
    }
}
