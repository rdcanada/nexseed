<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanymasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companymaster', function (Blueprint $table) {
            $table->id('cmpID');
            $table->string('cmpName');
            $table->string('cmmAddress');
            $table->string('cmpStatus');
            $table->string('cmpUpdated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companymaster');
    }
}
