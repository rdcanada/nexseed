<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/maintenance/company-index', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'index'])->name('company-index');
Route::get('/maintenance/company-list', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'list'])->name('company-list');
Route::get('/maintenance/company-info', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'info'])->name('company-info');
Route::post('/maintenance/company-store', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'store'])->name('company-store');
Route::post('/maintenance/company-update', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'update'])->name('company-update');
Route::post('/maintenance/company-cancel', [App\Http\Controllers\Maintenance\CompanyMasterController::class, 'cancel'])->name('company-cancel');
