<?php

namespace App\Models\Maintenance;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{
    use HasFactory;
    protected $table ="companymaster";
    protected $primaryKey ="cmpID";


  
   protected static function boot()
   {
       parent::boot();
   
       static::creating(function ($query) {
           $query->cmpStatus = 'A';
       });

       static::updating(function ($query) {
           $query->cmpUpdated_by = Auth::user()->id;
       });
   }

}
