<?php

namespace App\Http\Controllers\Maintenance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Maintenance\Company;
use Validator;
use Illuminate\Support\Facades\Auth;
class CompanyMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $pagename = 'Company Master';
        return view('maintenancemenu.companymaster',compact('pagename'));


    }

    public function list()
    {
        $companies = Company::all();
        return json_encode(['data'=>$companies]);
        

    }
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(),[
            'cmpName'=>'required',
            'cmpAddress'=>'required'

        ]);

        if($validation->passes())
        {
            $company = new Company();
            $company->cmpName = $request->cmpName;
            $company->cmpAddress = $request->cmpAddress;
            $company->cmpStatus = 'A';
            $company->save();

        }
        else
            return response()->json(['error'=>$validation->errors()->all()]);
    }

    public function info(Request $request)
    {
        $companyinfo = Company::find($request->id);
        
        return json_encode(['companyinfo'=>$companyinfo]);
    }
    public function update(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'cmpName'=>'required',
            'cmpAddress'=>'required'

        ]);
        if($validation->passes())
        {
            $company = Company::find($request->id);
            $company->cmpName = $request->cmpName;
            $company->cmpAddress = $request->cmpAddress;
            $company->save();
        }



    }
    public function cancel(Request $request)
    {
        $company = Company::find($request->id);
        $company->cmpStatus = 'C';
        $company->save();
    }
}
