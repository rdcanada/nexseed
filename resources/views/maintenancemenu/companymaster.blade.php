@extends('layouts.master')
@section('css')
<style>
#commonLoader { 
            position: absolute;
            left:0;
            top: 0;
            z-index: 3;
            margin: 0;
            border: 0px solid red;
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.90);
        }
.selected{
    background-color: #7bd1ed;

}
</style>
@endsection
@section('content')
<div class="col-md-12 col-lg-12">
    <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="float-right">
                <button type="button" id="btnAdd" class="btn btn-success">
                    <span class="fa fa-edit" aria-hidden="true"></span>&nbsp;Add
                </button>
                <button id= "btnEdit" type="button" class="btn btn-warning">
                    <span class="fa fa-edit" aria-hidden="true"></span>&nbsp;Edit
                </button>
                <button type="button" id="btnRemove" class="btn btn-danger">
                    <span class="fa fa-edit" aria-hidden="true"></span>&nbsp;Remove
                </button>
            </div>
    </diV>
</div>
<div class="col-sm-12 col-md-12 col-lg-12">
<table id="simpletable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Status</th>
                            </tr>
                        </thead>
 </table>
</div>
@endsection
@section('modal')
<div class="modal fade" id="maintenancetransactionmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form role="form" id="commonForm" method="POST" enctype="multipart/form-data" action="">
      {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="cmpID"></label>
                    <input type="text" class="form-control" id="cmpID" name="cmpID" placeholder="TEMP" readonly>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Name</label>
                    <input type="text" class="form-control" id="cmpName" name="cmpName" placeholder="Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Address</label>
                    <input type="text" class="form-control" id="cmpAddress" name="cmpAddress" placeholder="Address">
                  </div>
                  <!-- <select id="companyselect" class="form-control companyselect"></select> -->
                </div>
                <!-- /.card-body -->

                <!-- <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div> -->
                <div class="modal-footer">
              
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                <!-- <div class="row">  -->
                
            </div>
        </form>
        <div id="commonLoader" style="display:none;position:absolute;">
                            <div style="position:relative;top:40%;text-align:center;">
                                <div class='sk-wave'>
                                        <div class='sk-rect sk-rect1'></div><div class='sk-rect sk-rect2'></div><div class='sk-rect sk-rect3'></div><div class='sk-rect sk-rect4'></div><div class='sk-rect sk-rect5'></div><div class='sk-rect sk-rect6'></div><div class='sk-rect sk-rect7'></div><div class='sk-rect sk-rect8'></div>
                                </div>
             
                                <div id='commonLoaderCaption' style='font: 15px Sans-Serif;text-transform: initial;letter-spacing: -0.025em;text-align: center;' >
                                        Saving, Please wait...
                                </div>
                            </div>
                </div>
      </div>
      
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/customjs/maintenance/companymaster.js') }}" ></script> 


</script>
@endsection