var table = null;
var baseurl = window.location.origin;
var isEdit = false;
var selectedID = "";
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
    }
});
$(document).ready(function(){
    toastr.options = {
        'closeButton': true,
        'debug': false,
        'newestOnTop': false,
        'progressBar': false,
        'positionClass': 'toast-top-right',
        'preventDuplicates': false,
        'showDuration': '1000',
        'hideDuration': '1000',
        'timeOut': '5000',
        'extendedTimeOut': '1000',
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut',
    };

    //* table initialization **/

    table = $('#simpletable').DataTable({
        responsive: true,
        processing:true,
    // ajax: '{{route('time_mod-list')}}',
        ajax: {
                    "url": baseurl+"/maintenance/company-list",
                },
            "paging": true,
            "language":{
                "processing": "<div style='position:relative;top:30px;text-align: center;'><div class='sk-wave'><div class='sk-rect sk-rect1'></div><div class='sk-rect sk-rect2'></div><div class='sk-rect sk-rect3'></div><div class='sk-rect sk-rect4'></div><div class='sk-rect sk-rect5'></div><div class='sk-rect sk-rect6'></div><div class='sk-rect sk-rect7'></div><div class='sk-rect sk-rect8'></div></div><div style='font: 15px Sans-Serif;text-transform: initial;letter-spacing: -0.025em;text-align: center;' > Loading, Please wait... </div></div>"
            },
            "initComplete": function( settings, json ) {
                $('#loaderProgress').remove();
                $('#loaderCaption').remove();
            //  $('#tblUsers').DataTable().columns.adjust().responsive.recalc();
            
            },
        "columns": 
            [
                {"data": "cmpID"},
                {"data": "cmpName"},
                {"data": "cmpAddress"},
                {"data": "cmpStatus"}    
            
                    
            ],
            // "columnDefs": [
            //     {"className": "hidden", "targets": 0},
            //     {"className": "dt-left", "targets": 1},
            //     {"className": "dt-left", "targets": 2},
            //     {"className": "dt-left", "targets": 4},
            //     {"width": '0%', "targets": 0},
            //     {"width": '8%', "targets": 1},
            //     {"width": '8%', "targets": 2}, 
            //     {"width": '24%', "targets": 3},
            //     {"width": '15%', "targets": 4},
            //     {"width": '15%', "targets": 5},
            //     {"width": '15%', "targets": 6},
            //     {"width": '15%', "targets":7}
            
            
                
            
            // ],
            "createdRow": function( row, data, dataIndex){
                if( data['cmpStatus'] !=  'A'){
                    $(row).addClass('disabledStatus');
                }
            },
    });

    $('#simpletable').on('click', 'tr', function(){
        var selectedrow =$('#simpletable').DataTable().row(this);
        if($(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
            else {
                $('#simpletable').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
            }
        //selectedrow.addClass('selected');
        selectedID =  selectedrow.data().cmpID;
        
       
    });

    $('#btnAdd').click(function(e){
        isEdit = false;
        $('#maintenancetransactionmodal').modal('show');

    });
   
    $("#btnEdit").click(function(e){
        e.preventDefault();
        isEdit = true;
        id = selectedID;
        $.ajax({
                    url: baseurl+"/maintenance/company-info",
                    type: "get",
                    data:  {id: id},
                    dataType:"json",
                    cache: false,
                    beforeSend: function(){
                        $('#btnSave').prop('disabled', true );
                        $('#commonLoaderCaption').text('Retrieving details, Please wait...');
                        $('#commonLoader').show();
                                    
                    },
                    success: function(response){
                        $.each(response, function(index, element) {
                          
                            var key = Object.keys(element); 
                            
                            key.forEach(function(fieldname){  
                                $('#'+fieldname+'').val(element[fieldname]);
                               
                            });
                        });

                       
                        $('#commonLoader').hide();
                    }
                });                


        $("#maintenancetransactionmodal").modal('show');
    });
    $('#btnRemove').click(function(e){
        if ((table.data().any()==false) || (selectedID =='')) {
                return false;
        }

        var id=selectedID; 
        var route =   baseurl+"/maintenance/company-cancel"+id;
        confirmDialog("Are you sure you want to remove this record?", (ans) => {
        if (ans) {
                $.ajax( {
                            type        : 'post',
                            url         : baseurl+"/maintenance/company-cancel",                    
                            data: {
                                
                                "id": id
                                },
                            dataType    : 'json',
                            async       : false,
                            success     : function(data){
                               toastr.success("Successfully Deleted");
                                
                                tableTimeEntries.ajax.reload();
                            },
                            error: function(xhr, status, error){      
                                    var err = xhr.responseJSON;
                                
                                    toastr.error("Error encountered");
                            }
                            } );
        }
    });


    });
  

    $('#commonForm').on('submit',(function(e){
        e.preventDefault();
        var form = $('#commonForm').get(0); 
        var fieldValues = new FormData(form);

        var route = isEdit ? baseurl+"/maintenance/company-update" :  baseurl+"/maintenance/company-store";
        console.log(route);
            $.ajax( {
                                
                type        : 'POST',
                url         : route,
                data        : fieldValues,
                cache       : false,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $('#btnSave').prop('disabled', true );
                    document.getElementById("commonLoader").position = 'absolute'; 
                    $('#commonLoaderCaption').text('Saving, Please wait...');
                    $('#commonLoader').show();
                    
                },
                success     : function(data){
                    if($.isEmptyObject(data.error)){
                        $('#commonLoader').hide();
                        $('#maintenancetransactionmodal').modal("hide");
                        $('#commonForm')[0].reset();
                        $('#simpletable').DataTable().ajax.reload();
                        toastr.success("Successfuly Saved.");
                    }else{
                    
                        var message = '<br>';
                    
                        $.each(data.error, function(key, value){
                        message =  message +value+ '<br>';
                        });
                        toastr.error(message);
                        //showiBiznotification('fa fa-close','Error',message,'danger',400,1500);  
                        $('#commonLoader').hide();
                    }
                },
                error:function(xhr, status, error){ 
                    var msg = error;
                    toastr.error(msg);
                    $('#commonLoader').hide();
                    //showiBiznotification('fa fa-close','Error',msg,'danger',400,1000);  
                                               
                }
                
            });
           
            fieldValues = '';
            $('#btnSave').prop('disabled', false );


    }));

});


function confirmDialog(message, handler){
    $(`<div class="modal fade" id="myModal" role="dialog"> 
        <div class="modal-dialog modal-md"> 
            <!-- Modal content--> 
          <div class="modal-content"> 
            <div class="modal-header bg-primary" style="padding:10px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">Confirmation</h4>
            </div>
             <div class="modal-body" style="padding:10px;"> 
               <h4 class="text-center">${message}</h4> 
               <div class="text-center"> 
                 <a class="btn btn-danger btn-yes">Yes</a> 
                 <a class="btn btn-default btn-no">No</a> 
               </div> 
             </div> 
         </div> 
      </div> 
    </div>`).appendTo('body');
   
    //Trigger the modal
    $("#myModal").modal({
       backdrop: 'static',
       keyboard: false
    });
    
     //Pass true to a callback function
     $(".btn-yes").click(function () {
         handler(true);
         $("#myModal").modal("hide");
     });
      
     //Pass false to callback function
     $(".btn-no").click(function () {
         handler(false);
         $("#myModal").modal("hide");
     });

     //Remove the modal once it is closed.
     $("#myModal").on('hidden.bs.modal', function () {
        $("#myModal").remove(); 
     });
  }